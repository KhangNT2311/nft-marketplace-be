import aws from "aws-sdk";
import crypto from "crypto";
import { promisify } from "util";
const randomBytes = promisify(crypto.randomBytes);
const region = process.env.AMAZON_S3_REGION;
const accessKeyId = process.env.AMAZON_S3_ACCESS_KEY_ID;
const secretAccessKey = process.env.AMAZON_S3_SECRET_ACCESS_KEY;
const bucketName = process.env.AMAZON_S3_BUCKET;

const s3 = new aws.S3({
  region,
  accessKeyId,
  secretAccessKey,
  signatureVersion: "v4",
});

export const generateUploadURl = async () => {
  const rawBytes = await randomBytes(16);
  const key = rawBytes.toString("hex");
  const params = {
    Bucket: bucketName,
    Key: key,
    Expires: 60,
  };
  const uploadURL = await s3.getSignedUrlPromise("putObject", params);

  return uploadURL;
};

export const uploadFileS3Storage = async (file, publicId = "") => {
  const fileContent = Buffer.from(file.data, "binary");
  const rawBytes = await randomBytes(16);
  const key = publicId ? publicId?.split("/").at(-1) : rawBytes.toString("hex");
  console.log("key", key, publicId);
  const params = {
    Bucket: bucketName,
    Key: key,
    Body: fileContent,
    ContentType: file.mimetype,
  };
  try {
    await s3.putObject(params).promise();
    return `https://tribeblast-bucket.s3.us-east-2.amazonaws.com/${key}`;
  } catch (error) {
    throw error;
  }
};
export const deleteObjects = async (files) => {
  const options = {
    Bucket: bucketName,
    Delete: {
      Objects: files,
    },
  };
  return await s3.deleteObjects(options).promise();
};
