const allRoles = {
  user: [],
  admin: ['manageUsers', 'manageNews', 'manageParts', 'manageDrakerImages'],
};

const roles = Object.keys(allRoles);
const roleRights = new Map(Object.entries(allRoles));

export { roles, roleRights };
