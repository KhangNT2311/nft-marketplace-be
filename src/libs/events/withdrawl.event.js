import { getPoolContract } from '../../constant/contract';
import { EVENT_TYPES } from './utils';
import transactionService from '../../services/transaction.service';
import { ethers } from 'ethers';
import userService from '../../services/user.service';

export const handleWithdrawl = async (token, tokenAmount, userAddress, tx) => {
  try {
    const amount = ethers.utils.formatEther(tokenAmount);
    const user = await userService.updateWithdrawResult(amount, userAddress);
    if (user) {
      await transactionService.withdrawl(
        tx.transactionHash,
        userAddress,
        amount, user.lockedEmv

      );
    }


  } catch (error) {
    console.log(error);
  }
};

export const handleWithdrawlEvents = async () => {
  const poolContract = getPoolContract();

  poolContract.on(EVENT_TYPES.ExchangeTokenSecured, handleWithdrawl);
};
