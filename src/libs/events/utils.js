import mongoose from 'mongoose';

export const EVENT_TYPES = {
  BoxTransfer: 'TransferSingle',
  BoxTransferBatch: 'TransferBatch',
  DrakersTransfer: 'Transfer',
  MintWithRarity: 'MintWithRarity',
  NewListing: 'NewListing',
  PurchaseListing: 'PurchaseListing',
  CancelledListing: 'CancelledListing',
  ListingPriceChange: 'ListingPriceChange',
  BuyBoxLucky: 'BuyBoxLucky',
  ExchangeTokenSecured: 'ExchangeTokenSecured',
  MintNFTCustom: 'MintNFTCustom',
};

export const EVENT_NAMES = {
  BoxTransferBatch: 'BoxTransferBatch',
  BoxTransfer: 'BoxTransfer',
  DrakersTransfer: 'DrakersTransfer',
  MintWithRarity: 'MintWithRarity',
  NewListing: 'NewListing',
  PurchaseListing: 'PurchaseListing',
  CancelledListing: 'CancelledListing',
  ListingPriceChange: 'ListingPriceChange',
  ExchangeTokenSecured: 'ExchangeTokenSecured',
};

const { startSession } = mongoose;

export const wrapWithSession = async (fns, params) => {
  const session = await startSession();
  session.startTransaction();

  try {
    for (let i = 0; i < fns.length; i++) {
      await fns[i](session)(...params[i]);
    }
    await session.commitTransaction();
    await session.endSession();
    return true;
  } catch (error) {
    console.log('error : ', error);
    if (!session.hasEnded) {
      await session.abortTransaction();
      await session.endSession();
    }
    return false;
  }
};
