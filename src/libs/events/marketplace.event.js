import ethers from 'ethers';
import { getAddress, getMarketplaceContract } from '../../constant/contract';
import { MARKET_ITEM_TYPE, TRANSACTION_STATUS } from '../../constant/enums';
import marketService from '../../services/market.service';
import drakerService from '../../services/draker.service';
import { createTransaction, isExistTransaction } from '../../services/transaction.service';
import { EVENT_TYPES } from './utils';

export const handleNewListing =
  (session = null) =>
  async (listingId, seller, tokenId, listingNft, amount, totalPrice, paymentToken, listingTime, tx) => {
    try {
      const isExist = await isExistTransaction(tx?.transactionHash, tx?.logIndex);

      let drakersIdListing;

      if (!isExist) {
        const tokenIdCasted = tokenId.toString();
        const price = ethers.utils.formatEther(totalPrice);
        let itemType;

        if (listingNft.toLowerCase() === getAddress('drakers').toLowerCase()) {
          // draker
          itemType = MARKET_ITEM_TYPE.DRAKER;
          const drakerQuery = await drakerService.getDrakerByTokenId(tokenIdCasted);
          if (drakerQuery) {
            drakersIdListing = drakerQuery.tokenId;
            const data = await marketService.createListingItem(
              {
                listingId,
                owner: seller,
                draker: drakerQuery,
                marketType: MARKET_ITEM_TYPE.DRAKER,
                amount,
                price,
                paymentToken,
              },
              session
            );
          console.log('data', data);

          }
        }
        // else if (
        //   listingNft.toLowerCase() === getAddress('box').toLowerCase()
        // ) {
        //   if (tokenIdCasted === '0') {
        //     itemType = MARKET_ITEM_TYPE.GIFT_BOX;
        //   } else {
        //     itemType = MARKET_ITEM_TYPE.LUCKY_BOX;
        //   }

        //   await marketService.createListingItem(
        //     {
        //       listingId,
        //       owner: seller,
        //       marketType: itemType,
        //       amount,
        //       price,
        //       paymentToken,
        //     },
        //     session
        //   );
        // }
        let transactionBody = {
          owner: seller,
          seller,
          type: TRANSACTION_STATUS.SELL,
          itemType,
          price,
          txHash: tx?.transactionHash,
          logIndex: tx?.logIndex,
          paymentToken,
          unixTimestamp: Date.now(),
        };

        if (drakersIdListing) {
          transactionBody = { ...transactionBody, drakersId: drakersIdListing };
        }
        const transaction = await createTransaction(transactionBody, session);
      }
    } catch (error) {
      console.log('error handleNewListing', error);
    }
  };

export const handleCancelListing =
  (session = null) =>
  async (listingId, tx) => {
    try {
      const isExist = await isExistTransaction(tx?.transactionHash, tx?.logIndex);

      if (!isExist) {
        const listingItem = await marketService.getListingItem(+listingId);
        if (!listingItem) return;

        let transactionBody = {
          owner: listingItem.owner,
          type: TRANSACTION_STATUS.CANCEL,
          txHash: tx?.transactionHash,
          logIndex: tx?.logIndex,
          unixTimestamp: Date.now(),
        };

        await createTransaction(transactionBody, session);

        await marketService.deleteListingItem(+listingId, session);
      }
    } catch (error) {
      console.log('error handleCancelListing', error);
    }
  };

export const handlePurchaseListing =
  (session = null) =>
  async (listingId, buyer, seller, amount, listingNft, paymentToken, price, tx) => {
    try {
      const isExist = await isExistTransaction(tx?.transactionHash, tx?.logIndex);
      if (!isExist) {
        const listingItem = await marketService.getListingItem(+listingId);
        const { amount, marketType, price, paymentToken } = listingItem;
        // const userSell = await userService.getUserByAddress(listingItem.owner);

        let transactionBody = {
          owner: buyer,
          seller,
          type: TRANSACTION_STATUS.SOLD,
          itemType: marketType,
          price,
          txHash: tx?.transactionHash,
          logIndex: tx?.logIndex,
          amount,
          paymentToken,
          unixTimestamp: Date.now(),
        };

        if (marketType === MARKET_ITEM_TYPE.DRAKER) {
          transactionBody = {
            ...transactionBody,
            drakersId: listingItem.draker.tokenId,
          };
        }

        const transaction = await createTransaction(transactionBody, session);

        await marketService.deleteListingItem(+listingId, session);
      }
    } catch (error) {
      console.log('error handlePurchaseListing', error);
    }
  };

export const handleMarketplaceEvent = () => {
  const marketplaceContract = getMarketplaceContract();
  marketplaceContract.on(
    EVENT_TYPES.NewListing,
    (listingId, seller, tokenId, listingNft, amount, totalPrice, paymentToken, listingTime, tx) => {
      handleNewListing()(listingId, seller, tokenId, listingNft, amount, totalPrice, paymentToken, listingTime, tx);
    }
  );
  marketplaceContract.on(EVENT_TYPES.CancelledListing, (listingId, tx) => {
    handleCancelListing()(listingId, tx);
  });
  marketplaceContract.on(
    EVENT_TYPES.PurchaseListing,
    (listingId, buyer, seller, amount, listingNft, paymentToken, price, tx) => {
      handlePurchaseListing()(listingId, buyer, seller, amount, listingNft, paymentToken, price, tx);
    }
  );
};
