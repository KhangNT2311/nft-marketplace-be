import { handleTransferEvent } from './transfer.event';
import { handleMarketplaceEvent } from './marketplace.event';
import { handleLostEvents } from './missing.event';
import { handleWithdrawlEvents } from './withdrawl.event';
import { handleSaleEvent } from './sale.event';
export default {
  handleMarketplaceEvent,
  handleTransferEvent,
  handleLostEvents,
  handleWithdrawlEvents,
  handleSaleEvent,
};
