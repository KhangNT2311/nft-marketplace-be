import { ADDRESSES } from '../../constant/address';
import { getSaleContract } from '../../constant/contract';
import { TRANSACTION_STATUS } from '../../constant/enums';
import { Draker, User } from '../../models';
import { drakerService } from '../../services';
import { createTransaction, isExistTransaction } from '../../services/transaction.service';
import { EVENT_TYPES } from './utils';

export const handleOrderNFT =
  (session = null) =>
  async (amount, paymentToken, rarity, userMintNonce, metadata, tokenId, tx) => {
    try {
      console.log('listen handleOrder NFT');
      const isExist = await isExistTransaction(tx?.transactionHash, tx?.logIndex);

      let drakersIdListing;

      if (!isExist) {
        const metadataObj = JSON.parse(metadata);
        console.log('metadataObj', metadataObj);
        const drakerBody = {
          ...metadataObj,
          tokenId: tokenId.toString(),
          nftAddress: ADDRESSES['drakers'][process.env.CHAIN_ID],
        };
        const draker = await Draker.create(drakerBody);
        // const draker = await drakerService.createDraker(drakerBody, session);
        // else if (
        //   listingNft.toLowerCase() === getAddress('box').toLowerCase()
        // ) {
        //   if (tokenIdCasted === '0') {
        //     itemType = MARKET_ITEM_TYPE.GIFT_BOX;
        //   } else {
        //     itemType = MARKET_ITEM_TYPE.LUCKY_BOX;
        //   }

        //   await marketService.createListingItem(
        //     {
        //       listingId,
        //       owner: seller,
        //       marketType: itemType,
        //       amount,
        //       price,
        //       paymentToken,
        //     },
        //     session
        //   );
        // }
        console.log('draker1', draker);
        await User.findOneAndUpdate(
          { walletAddress: metadataObj.owner },
          {
            $inc: {
              mintNonce: 1,
            },
          },
          {
            upsert: true,
            new: true,
          }
        );
        console.log('draker', draker);
        let transactionBody = {
          owner: metadata.owner,
          type: TRANSACTION_STATUS.ORDER_NFT,
          price: amount.toString(),
          txHash: tx?.transactionHash,
          logIndex: tx?.logIndex,
          paymentToken,
          unixTimestamp: Date.now(),
          drakersId: draker,
        };
        const transaction = await createTransaction(transactionBody, session);
      }
    } catch (error) {
      console.log('error handleNewListing', error);
    }
  };

export const handleSaleEvent = () => {
  const saleContract = getSaleContract();
  saleContract.on(EVENT_TYPES.MintNFTCustom, (amount, paymentToken, rarity, userMintNonce, metadata, tokenId, tx) => {
    handleOrderNFT()(amount, paymentToken, rarity, userMintNonce, metadata, tokenId, tx);
  });
};
