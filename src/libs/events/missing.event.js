/* eslint-disable */
import {
  defaultProvider,
  getBoxContract,
  getBoxInteractionContract,
  getMarketplaceContract,
  getPoolContract,
  getDrakersContract,
} from '../../constant/contract';
import SyncedEventModel from '../../models/synced-event.model';
import { handleCancelListing, handleNewListing, handlePurchaseListing } from './marketplace.event';
import { handleMintDrakers, handleDrakersTransfer } from './transfer.event';
import { EVENT_NAMES, EVENT_TYPES } from './utils';
import { handleWithdrawl } from './withdrawl.event';

const getStartBlock = async (eventType) => {
  const latestSyncedEvent = await SyncedEventModel.findOne(
    {},
    {},
    {
      sort: {
        latest: 1,
      },
    }
  );
  if (!latestSyncedEvent) {
    console.log(`missing latest synced block when get start event: ${eventType}`);

    const savedEvents = await SyncedEventModel.find({}, { name: 1 });
    const savedEventNames = savedEvents.map((event) => event.name);
    const eventToBeCreated = [];

    Object.values(EVENT_NAMES).forEach((name) => {
      if (!savedEventNames.includes(name)) {
        eventToBeCreated.push({
          name,
          latest: process.env.START_BLOCK_SYNC_EVENT || 20211210,
        });
      }
    });

    await SyncedEventModel.insertMany(eventToBeCreated);

    return { startBlock: process.env.START_BLOCK_SYNC_EVENT || 20502347 };
  }

  return { startBlock: latestSyncedEvent.latest };
};

const updateLatestBlock =
  (session = null) =>
  async (eventType, value) => {
    const latestSyncedEvent = await SyncedEventModel.findOne({
      name: eventType,
    });

    if (!latestSyncedEvent) {
      console.log(`missing latest synced block when update event: ${eventType}`);
      await SyncedEventModel.create(
        {
          name: eventType,
          latest: value,
        },
        { session }
      );
      return;
    }

    await SyncedEventModel.updateOne({ name: eventType }, { latest: value }, { session });
  };

const boxContract = getBoxContract();
const drakersContract = getDrakersContract();
const boxInteractionContract = getBoxInteractionContract();
const marketplaceContract = getMarketplaceContract();
const poolContract = getPoolContract();

const queryByOrder = async () => {
  try {
    const { startBlock } = await getStartBlock(EVENT_NAMES.BoxTransferBatch);
    const latestBlock = await defaultProvider.getBlockNumber();

    let currentBlock = startBlock;
    console.log(`queryByOrder: currentBlock: `, currentBlock, await defaultProvider.getBlockNumber(), new Date());

    while (currentBlock < latestBlock) {
      const toBlock = currentBlock + 5000 < latestBlock ? currentBlock + 5000 : latestBlock;

      //====== BOX TRANSFER BATCH ======
      const responseTransferBatchBox = await boxContract.queryFilter(
        boxContract.filters[EVENT_TYPES.BoxTransferBatch](),
        currentBlock,
        toBlock
      );

      for (const tx of responseTransferBatchBox) {
        const { operator, from, to, ids, values } = tx.args;

        await handleBoxTransferBatch()(operator, from, to, ids, values, tx);
      }

      await updateLatestBlock()(EVENT_NAMES.BoxTransferBatch, toBlock);

      //====== BOX TRANSFER ======
      const responsesTransferBox = await boxContract.queryFilter(
        boxContract.filters[EVENT_TYPES.BoxTransfer](),
        currentBlock,
        toBlock
      );

      for (const tx of responsesTransferBox) {
        const { operator, from, to, id, value } = tx.args;

        await handleBoxTransfer()(operator, from, to, id, value, tx);
      }

      await updateLatestBlock()(EVENT_NAMES.BoxTransfer, toBlock);

      //======= MINT DRAKERS ========
      const responseMintDrakers = await drakersContract.queryFilter(
        drakersContract.filters[EVENT_TYPES.MintWithRarity](),
        currentBlock,
        toBlock
      );

      for (const tx of responseMintDrakers) {
        const { user, tokenId, rarity } = tx.args;

        await handleMintDrakers()(user, tokenId, rarity, tx);
      }

      await updateLatestBlock()(EVENT_NAMES.MintWithRarity, toBlock);

      //======== DRAKERS TRANSFER =========
      const responsesTransferDrakers = await drakersContract.queryFilter(
        drakersContract.filters[EVENT_TYPES.DrakersTransfer](),
        currentBlock,
        toBlock
      );

      for (const tx of responsesTransferDrakers) {
        const { from, to, tokenId } = tx.args;
        await handleDrakersTransfer()(from, to, tokenId, tx);
      }

      await updateLatestBlock()(EVENT_NAMES.DrakersTransfer, toBlock);

      //========== NEW LISTING ==========
      const responseNewListing = await marketplaceContract.queryFilter(
        marketplaceContract.filters[EVENT_TYPES.NewListing](),
        currentBlock,
        toBlock
      );

      for (const tx of responseNewListing) {
        const { listingId, seller, tokenId, listingNft, amount, totalPrice, paymentToken, listingTime } = tx.args;

        await handleNewListing()(listingId, seller, tokenId, listingNft, amount, totalPrice, paymentToken, listingTime, tx);
      }

      await updateLatestBlock()(EVENT_NAMES.NewListing, toBlock);

      //======= CANCEL LISTING =======

      const responsesCancel = await marketplaceContract.queryFilter(
        marketplaceContract.filters[EVENT_TYPES.CancelledListing](),
        currentBlock,
        toBlock
      );

      for (const tx of responsesCancel) {
        const { listingId } = tx.args;

        await handleCancelListing()(listingId, tx);
      }

      await updateLatestBlock()(EVENT_NAMES.CancelledListing, toBlock);

      //======== PURCHASE LISTING ==========

      const responsesPurchase = await marketplaceContract.queryFilter(
        marketplaceContract.filters[EVENT_TYPES.PurchaseListing](),
        currentBlock,
        toBlock
      );

      for (const tx of responsesPurchase) {
        const { listingId, buyer, seller, amount, listingNft, paymentToken, price } = tx.args;

        await handlePurchaseListing()(listingId, buyer, seller, amount, listingNft, paymentToken, price, tx);
      }

      await updateLatestBlock()(EVENT_NAMES.PurchaseListing, toBlock);
      //========================================

      // ======= WITHDRAW ===========
      const responsesExchangeTokenSecured = await poolContract.queryFilter(
        poolContract.filters[EVENT_TYPES.ExchangeTokenSecured](),
        currentBlock,
        toBlock
      );

      for (const tx of responsesExchangeTokenSecured) {
        const { token, tokenAmount, userAddress } = tx.args;
        await handleWithdrawl(token, tokenAmount, userAddress, tx);
      }

      await updateLatestBlock()(EVENT_NAMES.ExchangeTokenSecured, toBlock);

      currentBlock = toBlock;
    }
  } catch (error) {
    console.log(`error when query by order missing event: ${error}`);
  }
};

export const handleLostEvents = () => {
  queryByOrder();
};
