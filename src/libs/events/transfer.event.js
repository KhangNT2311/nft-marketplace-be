import { ethers } from 'ethers';
import { ADDRESSES } from '../../constant/address';
import { getBoxContract, getMarketplaceContract, getDrakersContract } from '../../constant/contract';
import { DRAKER_TYPE, DRAKER_TYPE_IMAGE, TRANSACTION_STATUS } from '../../constant/enums';
import drakerService from '../../services/draker.service';
import userService from '../../services/user.service';
import transactionService from '../../services/transaction.service';
import { EVENT_TYPES, wrapWithSession } from './utils';
import { eventEmitter } from '../emitter';
import drakerImageService from '../../services/drakerImage.service';

export const handleDrakersTransfer = (session) => async (from, to, tokenId, tx) => {
  if (from !== ethers.constants.AddressZero) {
    const isSuccess = await transactionService.createTransaction(
      {
        logIndex: tx.logIndex,
        txHash: tx.transactionHash,
        owner: from.toLowerCase(),
        type: TRANSACTION_STATUS.TRANSFER_DRAKERS,
        unixTimestamp: Date.now(),
      },
      session
    );

    if (isSuccess) {
      if (to === ethers.constants.AddressZero) {
        await drakerService.deleteDraker(tokenId, session);
      } else {
        await drakerService.transferOwner(tokenId, from, to, session);
      }
    }
  }
};

export const handleOpenDrakers = (session) => async (user, drakersRarity, drakersId, tx) => {
  const intRarity = +drakersRarity;
  const isSuccess = await transactionService.createTransaction(
    {
      logIndex: tx.logIndex,
      txHash: tx.transactionHash,
      owner: user.toLowerCase(),
      type: TRANSACTION_STATUS.CREATE_DRAKERS,
      unixTimestamp: Date.now(),
    },
    session
  );

  if (isSuccess) {
    const attributeObj = drakerService.getRandomAttribute(intRarity);

    await drakerService.createDraker(
      {
        tokenId: drakersId,
        nftAddress: ADDRESSES['drakers'][process.env.CHAIN_ID],
        drakerType: DRAKER_TYPE[intRarity],
        owner: user.toLowerCase(),
        drakerImage: DRAKER_TYPE_IMAGE[intRarity],
        ...attributeObj,
      },
      session
    );
  }
};

export const handleMintDrakers = (session) => async (user, tokenId, rarity, tx) => {
  try {
    const intRarity = +rarity;
    console.log('draker intRarity', intRarity, tx);

    const isSuccess = await transactionService.createTransaction(
      {
        logIndex: tx.logIndex,
        txHash: tx.transactionHash,
        owner: user.toLowerCase(),
        type: TRANSACTION_STATUS.CREATE_DRAKERS,
        unixTimestamp: Date.now(),
      },
      session
    );
    console.log('isSuccess', isSuccess);
    if (isSuccess) {
      const attributeObj = drakerService.getRandomAttribute(intRarity);
      const parts = await drakerService.getRandomPartsByRarity(intRarity);
      const partIds = parts.map((item) => item._id);

      const drakerClass = drakerService.getRandomClass();
      const [draker] = await drakerService.createDraker(
        {
          tokenId,
          nftAddress: ADDRESSES['drakers'][process.env.CHAIN_ID],
          owner: user.toLowerCase(),
          drakerRarity: intRarity,
          parts: partIds,
          ...drakerClass,
          ...attributeObj,
        },
        session
      );
      const drakerImage = await drakerImageService.findByParts(partIds);
      console.log('drakerImageeee', drakerImage, draker);
      eventEmitter.emit(tx.transactionHash, { ...draker._doc, drakerImage: drakerImage?.imageUrl });
    }
  } catch (error) {
    console.log('minterror', error);
  }
};

export const handleTransferEvent = async () => {
  const drakersContract = getDrakersContract();

  drakersContract.on(EVENT_TYPES.MintWithRarity, (user, tokenId, rarity, tx) => {
    handleMintDrakers()(user, tokenId, rarity, tx);
  });

  drakersContract.on(EVENT_TYPES.DrakersTransfer, (from, to, tokenId, tx) => {
    handleDrakersTransfer()(from, to, tokenId, tx);
  });
};
