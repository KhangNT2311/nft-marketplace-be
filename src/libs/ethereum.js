import ethereumJsUtil from 'ethereumjs-utils';
import ethers from 'ethers';
import { BAD_REQUEST, createError } from '../common/error-utils';

const decodeSignature = (signature, msg) => {
  const msgBuffer = ethereumJsUtil.toBuffer(msg);
  const msgHash = ethereumJsUtil.hashPersonalMessage(msgBuffer);
  const signatureBuffer = ethereumJsUtil.toBuffer(signature);
  const signatureParams = ethereumJsUtil.fromRpcSig(signatureBuffer);
  const publicKey = ethereumJsUtil.ecrecover(
    msgHash,
    signatureParams.v,
    signatureParams.r,
    signatureParams.s
  );
  const addressBuffer = ethereumJsUtil.publicToAddress(publicKey);
  const address = ethereumJsUtil.bufferToHex(addressBuffer);

  return address;
};

const getMessageAuthenticationHash = (account, nonce) => {
  return ethers.utils.arrayify(
    ethers.utils.solidityKeccak256(['address', 'uint256'], [account, nonce])
  );
};

const signMessage = async (privateKey, message) => {
  const signer = new ethers.Wallet(privateKey);

  return signer.signMessage(message);
};

const verifyMessage = (messageHash, signature) => {
  return ethers.utils.verifyMessage(messageHash, signature);
};

const verifyAuthenticationMessage = (signature, account, nonce) => {
  const messageHash = getMessageAuthenticationHash(account, nonce);

  const accountRecover = verifyMessage(messageHash, signature);
  if (accountRecover !== account) {
    throw createError(BAD_REQUEST, 'Please use correct account to sign');
  }
};

export default {
  decodeSignature,
  getMessageHash: getMessageAuthenticationHash,
  signMessage,
  verifyMessage,
  verifyAuthenticationMessage,
};
