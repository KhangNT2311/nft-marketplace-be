import mongoose from 'mongoose';
import { paginate } from './plugins';
import toJSON from './plugins/toJSON.plugin';

const { Schema } = mongoose;

const DrakerImageSchema = new Schema(
  {
    imageUrl: String,
    parts: [{ type: Schema.Types.ObjectId, ref: 'parts' }],
  },
  { timestamps: true }
);

DrakerImageSchema.plugin(toJSON);
DrakerImageSchema.plugin(paginate);

const DrakerImage = mongoose.model('draker-images', DrakerImageSchema);

export default DrakerImage;
