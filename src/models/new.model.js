import mongoose from 'mongoose';

import { paginate, toJSON } from './plugins';

const { Schema } = mongoose;
const newSchema = new Schema(
  {
    author: { type: String, ref: 'User' },
    title: String,
    content: String,
    thumbnail: String,
  },
  { timestamps: true }
);
newSchema.plugin(toJSON);
newSchema.plugin(paginate);

const New = mongoose.model('news', newSchema);

export default New;
