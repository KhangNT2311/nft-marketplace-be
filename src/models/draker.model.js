import mongoose from 'mongoose';
import { DRAKER_TYPES } from '../constant/enums';

import { paginate, toJSON } from './plugins';

const { Schema } = mongoose;

const drakerSchema = new Schema(
  {
    tokenId: {
      type: String,
      unique: true,
      required: true,
    },
    nftAddress: String,
    price: {
      type: Number,
      default: 0,
    },
    description: String,
    mintDate: Date,
    owner: { type: String, lowercase: true },
    // drakerImage: String,
    paymentToken: String,
    isSelected: {
      type: Boolean,
      default: false,
    },
    level: {
      type: Number,
      default: 1,
    },
    drakerClass: {
      type: String,
    },
    parts: [{ type: Schema.Types.ObjectId, ref: 'parts' }],
    // drakerImage: { type: Schema.Types.ObjectId, ref: 'draker-images' },
    attributeElements: {
      attack: Number,
      defend: Number,
      speed: Number,
    },
    drakerRarity: Number,
  },
  { timestamps: true }
);
drakerSchema.plugin(toJSON);
drakerSchema.plugin(paginate);

const Draker = mongoose.model('drakers', drakerSchema);

export default Draker;
