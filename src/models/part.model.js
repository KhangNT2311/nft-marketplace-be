import mongoose from 'mongoose';
import { DRAKER_PARTS } from '../constant/enums';
import { paginate } from './plugins';
import toJSON from './plugins/toJSON.plugin';

const { Schema } = mongoose;

const PartSchema = new Schema(
  {
    name: String,
    type: {
      type: String,
      enum: DRAKER_PARTS,
    },
    price: Number,
    rarity: Number,
    imageUrl: String,
  },
  { timestamps: true }
);

PartSchema.plugin(toJSON);
PartSchema.plugin(paginate);

const Part = mongoose.model('parts', PartSchema);

export default Part;
