import mongoose from 'mongoose';

import { EVENT_NAMES } from '../libs/events/utils';
import toJSON from './plugins/toJSON.plugin';

const { Schema } = mongoose;

const SyncedEventSchema = new Schema(
  {
    name: {
      type: String,
      enum: Object.values(EVENT_NAMES),
      require: true,
      unique: true,
    },
    latest: Number,
  },
  { timestamps: true }
);

SyncedEventSchema.plugin(toJSON);

const SyncedEvent = mongoose.model('synced-events', SyncedEventSchema);

export default SyncedEvent;
