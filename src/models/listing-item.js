import mongoose from 'mongoose';
import { MARKET_ITEM_TYPES } from '../constant/enums';
import paginate from './plugins/paginate.plugin';
import toJSON from './plugins/toJSON.plugin';

const { Schema } = mongoose;

const ListingItemSchema = new Schema(
  {
    listingId: { type: Number, required: true, unique: true },
    owner: { type: String },
    draker: { type: Schema.Types.ObjectId, ref: 'drakers' },
    marketType: {
      type: String,
      enum: MARKET_ITEM_TYPES,
    },
    paymentToken: {
      type: String,
    },
    amount: Number,
    price: { type: Number },
  },
  { timestamps: true }
);

ListingItemSchema.plugin(toJSON);
ListingItemSchema.plugin(paginate);

const ListingItem = mongoose.model('listing-items', ListingItemSchema);

export default ListingItem;
