import mongoose from 'mongoose';
import { paginate } from './plugins';
import toJSON from './plugins/toJSON.plugin';

const { Schema } = mongoose;

const PartTypeSchema = new Schema(
  {
    name: String,
  },
  { timestamps: true }
);

PartTypeSchema.plugin(toJSON);
PartTypeSchema.plugin(paginate);

const PartType = mongoose.model('part-types', PartTypeSchema);

export default PartType;
