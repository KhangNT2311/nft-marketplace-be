import mongoose from 'mongoose';
import { MARKET_ITEM_TYPES, TRANSACTION_STATUSES } from '../constant/enums';
import paginate from './plugins/paginate.plugin';
import toJSON from './plugins/toJSON.plugin';

const { Schema } = mongoose;

const MarketTransactionSchema = new Schema(
  {
    _id: {
      type: String,
      unique: true,
      required: true,
    },
    owner: { type: String },
    seller: String,
    type: {
      type: String,
      enum: TRANSACTION_STATUSES,
    },
    itemType: {
      type: String,
      enum: MARKET_ITEM_TYPES,
    },
    price: String,
    txHash: {
      type: String,
      required: true,
    },
    drakersId: String,
    amount: Number,
    paymentToken: String,
    unixTimestamp: Number,
  },
  { timestamps: true },
  { _id: false }
);

MarketTransactionSchema.plugin(toJSON);
MarketTransactionSchema.plugin(paginate);

const MarketTransaction = mongoose.model('market-transactions', MarketTransactionSchema);

export default MarketTransaction;
