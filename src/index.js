import mongoose from 'mongoose';
import app from './app';
import config from './config/config';
import logger from './config/logger';
import eventHandler from './libs/events';
import { Draker } from './models';
import { drakerService } from './services';

let server;
mongoose.connect(config.mongoose.url, config.mongoose.options).then(() => {
  logger.info('Connected to MongoDB');
  server = app.listen(config.port, async () => {
    logger.info(`Listening to port ${config.port}`);
    eventHandler.handleTransferEvent();
    eventHandler.handleMarketplaceEvent();
    eventHandler.handleWithdrawlEvents();
    eventHandler.handleSaleEvent();
    // const drakers = await Draker.find({});
    // // // eslint-disabled-next-line
    // for (const draker of drakers) {
    //   draker.parts = await drakerService.getRandomPartsByRarity(1);
    //   await draker.save();
    // }
    // console.log("donneee")
    // const drakerImages = await DrakerImage.find({});
    // for (const drakerImage of drakerImages) {
    //   const items = drakerImage.
    // }
    // eventHandler.handleLostEvents();
  });
});

const exitHandler = () => {
  if (server) {
    server.close(() => {
      logger.info('Server closed');
      process.exit(1);
    });
  } else {
    process.exit(1);
  }
};

const unexpectedErrorHandler = (error) => {
  logger.error(error);
  exitHandler();
};

process.on('uncaughtException', unexpectedErrorHandler);
process.on('unhandledRejection', unexpectedErrorHandler);

process.on('SIGTERM', () => {
  logger.info('SIGTERM received');
  if (server) {
    server.close();
  }
});
