import httpStatus from 'http-status';
import New from '../models/new.model';
import ApiError from '../utils/ApiError';

/**
 * Query for newCreateds
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryNews = async (filter, options) => {
  const newCreateds = await New.paginate(filter, options);
  return newCreateds;
};

const createNew = async (newCreatedBody) => {
  return New.create(newCreatedBody);
};
const getNewById = async (id) => {
  return New.findById(id);
};
const updateNewById = async (newCreatedId, updateBody) => {
  const newCreated = await getNewById(newCreatedId);
  if (!newCreated) {
    throw new ApiError(httpStatus.NOT_FOUND, 'New not found');
  }
  Object.assign(newCreated, updateBody);
  await newCreated.save();
  return newCreated;
};
const deleteNewById = async (newCreatedId) => {
  const newCreated = await getNewById(newCreatedId);
  if (!newCreated) {
    throw new ApiError(httpStatus.NOT_FOUND, 'New not found');
  }
  await newCreated.remove();
  return newCreated;
};
export default {
  queryNews,
  createNew,
  deleteNewById,
  updateNewById,
};
