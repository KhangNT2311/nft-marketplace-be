const getBoxType = (tokenId) => {
  if (tokenId === 0) {
    return 'gift';
  }
  return 'lucky';
};
export default {
  getBoxType,
};
