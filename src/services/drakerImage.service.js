import httpStatus from 'http-status';
import DrakerImage from '../models/drakerImage.model';
import ApiError from '../utils/ApiError';

/**
 * Query for drakerImageCreateds
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryDrakerImages = async (filter, options) => {
  const drakerImageCreateds = await DrakerImage.paginate(filter, options);
  return drakerImageCreateds;
};

const createDrakerImage = async (drakerImageCreatedBody) => {
  return DrakerImage.create(drakerImageCreatedBody);
};
const getDrakerImageById = async (id) => {
  return DrakerImage.findById(id);
};
const updateDrakerImageById = async (drakerImageCreatedId, updateBody) => {
  const drakerImageCreated = await getDrakerImageById(drakerImageCreatedId);
  if (!drakerImageCreated) {
    throw new ApiError(httpStatus.NOT_FOUND, 'DrakerImage not found');
  }
  Object.assign(drakerImageCreated, updateBody);
  await drakerImageCreated.save();
  return drakerImageCreated;
};
const deleteDrakerImageById = async (drakerImageCreatedId) => {
  const drakerImageCreated = await getDrakerImageById(drakerImageCreatedId);
  if (!drakerImageCreated) {
    throw new ApiError(httpStatus.NOT_FOUND, 'DrakerImage not found');
  }
  await drakerImageCreated.remove();
  return drakerImageCreated;
};

const findByParts = async (parts) => {
  const drakerImage = await DrakerImage.findOne({
    parts,
  });

  return drakerImage;
};

export default {
  queryDrakerImages,
  createDrakerImage,
  deleteDrakerImageById,
  updateDrakerImageById,
  findByParts,
};
