import httpStatus from 'http-status';
import Part from '../models/part.model';
import ApiError from '../utils/ApiError';

/**
 * Query for partCreateds
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryParts = async (filter, options) => {
  const partCreateds = await Part.paginate(filter, options);
  return partCreateds;
};

const createPart = async (partCreatedBody) => {
  return Part.create(partCreatedBody);
};
const getPartById = async (id) => {
  return Part.findById(id);
};
const updatePartById = async (partCreatedId, updateBody) => {
  const partCreated = await getPartById(partCreatedId);
  if (!partCreated) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Part not found');
  }
  Object.assign(partCreated, updateBody);
  await partCreated.save();
  return partCreated;
};
const deletePartById = async (partCreatedId) => {
  const partCreated = await getPartById(partCreatedId);
  if (!partCreated) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Part not found');
  }
  await partCreated.remove();
  return partCreated;
};
const findPartsByTypeAndRarity = async (type, rarity) => {
  const parts = await Part.find({
    type,
    rarity,
  }).lean();
  if (!parts) return [];
  return parts;
};
export default {
  queryParts,
  createPart,
  deletePartById,
  updatePartById,
  findPartsByTypeAndRarity,
  getPartById,
};
