import MarketplaceTransaction from '../models/market-transaction.model';
import drakerService from './draker.service';

export const isExistTransaction = async (txHash, logIndex) => {
  let transaction = await MarketplaceTransaction.findById(`${txHash}-${logIndex}`);
  return !!transaction;
};

export const createTransaction = async (transactionBody, session = null) => {
  try {
    const { txHash, logIndex } = transactionBody;
    const transactionId = `${txHash}-${logIndex}`;

    const tx = await MarketplaceTransaction.findById(transactionId);
    if (!tx) {
      await MarketplaceTransaction.create(
        [
          {
            ...transactionBody,
            _id: transactionId,
          },
        ],
        { session }
      );

      return true;
    }

    return false;
  } catch (error) {
    return false;
  }
};

const queryTransactionsPublic = async (filter, options) => {
  const transactions = await MarketplaceTransaction.paginate(filter, options);
  const arrPromises = [];
  const arrIndexes = [];
  transactions?.results?.forEach((transaction, idx) => {
    if (transaction?.drakersId) {
      arrIndexes.push(idx);
      arrPromises.push(drakerService.getDrakerByTokenId(transaction?.drakersId));
    }
  });
  const data = await Promise.all(arrPromises);
  arrIndexes.forEach((item, idx) => {
    transactions.results[item]._doc.draker = data[idx];
  });
  return transactions;
};

export default {
  isExistTransaction,
  createTransaction,
  queryTransactionsPublic,
};

export const updateTransaction = async (id, bodyUpdate) => {
  return MarketplaceTransaction.findByIdAndUpdate(id, bodyUpdate);
};
