import { ethers } from 'ethers';
import httpStatus from 'http-status';
import ApiError from '../utils/ApiError';

const getMessageAuthenticationHash = async (account, nonce) => {
  return ethers.utils.solidityKeccak256(['address', 'uint256'], [account, nonce]);
};

const signMessage = async (privateKey, message) => {
  const signer = new ethers.Wallet(privateKey);

  return signer.signMessage(message);
};

const verifyMessage = async (messageHash, signature) => {
  return ethers.utils.verifyMessage(messageHash, signature);
};

const verifyAuthenticationMessage = async (signature, account, nonce) => {
  const messageHash = await getMessageAuthenticationHash(account, nonce);

  console.log('messageHash', messageHash, signature);

  const accountRecover = await verifyMessage(messageHash, signature);

  console.log('accountRecover', accountRecover, account);
  if (accountRecover !== account) {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Please use correct account to sign');
  }
};

export default {
  getMessageHash: getMessageAuthenticationHash,
  signMessage,
  verifyMessage,
  verifyAuthenticationMessage,
};
