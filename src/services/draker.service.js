import mongoose from 'mongoose';
import { ATTACK_ATTR, BEAST_PART, BODY_PART, CLASS_ATTR, DEFEND_ATTR, SPEED_ATTR, TAIL_PART } from '../constant/attribute';
import DRAKER_IMAGE from '../constant/drakers';
import { ListingItem } from '../models';
import Draker from '../models/draker.model';
import marketService from '../services/market.service';
import drakerImageService from './drakerImage.service';
import partService from './part.service';
const createDraker = async (drakerBody, session = null) => {
  try {
    return await Draker.create([drakerBody], { session });
  } catch (error) {
    return false;
  }
};

const deleteDraker = async (tokenId, session = null) => {
  return Draker.findOneAndDelete(
    {
      tokenId,
    },
    { session }
  );
};

const getDrakerById = async (id) => {
  return Draker.findById(id);
};

const getDrakerByTokenId = async (tokenId) => {
  const draker = await Draker.findOne({ tokenId });
  const drakerImage = getDrakerImage(draker.parts);
  const drakerListing = await ListingItem.findOne({ draker: draker._id }).populate('draker');
  return {
    ...draker._doc,
    drakerImage,
    price: drakerListing?.price,
    paymentToken: drakerListing?.paymentToken,
  };
};

const updateDrakerById = async (drakerId, updateBody) => {
  const draker = await getDrakerById(drakerId);
  if (draker) {
    Object.assign(draker, updateBody);
    await draker.save();
    return draker;
  }
  return null;
};
const getDrakerImage = (parts) => {
  const { tail, beast, body } = parts;
  const tailPart = DRAKER_IMAGE.find((item) => item.value === tail);
  const beastPart = tailPart?.data?.find((item) => item.value === beast);
  const bodyPart = beastPart?.data?.find((item) => item.value === body);
  return bodyPart?.image;
};

const getDrakers = async (filter, options) => {
  const { results, ...others } = await Draker.paginate(filter, options);
  const mappedResult = [];
  for (const draker of results) {
    const { parts, ...otherDrakerProperties } = draker._doc;
    const partIds = parts.map((item) => item._id);
    const drakerImage = await drakerImageService.findByParts(partIds);
    mappedResult.push({
      ...otherDrakerProperties,
      parts,
      drakerImage: drakerImage?.imageUrl,
    });
  }
  return {
    results: mappedResult,
    ...others,
  };
};

export const transferOwner = async (tokenId, from, to, session = null) => {
  try {
    const drakers = await Draker.findOneAndUpdate(
      {
        tokenId,
      },
      {
        owner: to.toLowerCase(),
      },
      { session, new: true }
    );

    return drakers;
  } catch (error) {
    return false;
  }
};

const getRandomAttribute = (intRarity) => {
  const attrs = [ATTACK_ATTR, DEFEND_ATTR, SPEED_ATTR];

  const attrsValue = attrs.map((attr) => {
    const rangeAttribute = attr[intRarity];
    return Math.round(Math.random() * (rangeAttribute[1] - rangeAttribute[0])) + rangeAttribute[0];
  });

  return {
    attributeElements: {
      attack: attrsValue[0],
      defend: attrsValue[1],
      speed: attrsValue[2],
    },
  };
};
const getRandomParts = () => {
  const parts = [TAIL_PART, BEAST_PART, BODY_PART];
  const [tail, beast, body] = parts.map((attr) => {
    const randomNumber = Math.round(Math.random() * (attr.length - 1));

    return attr[randomNumber];
  });
  return {
    parts: {
      tail,
      beast,
      body,
    },
  };
};
const getRandomClass = () => {
  const randomNumber = Math.round(Math.random() * (CLASS_ATTR.length - 1));
  return {
    drakerClass: CLASS_ATTR[randomNumber],
  };
};

const getRandomPartsByRarity = async (rarity) => {
  const parts = ['tail', 'beast', 'body'];
  const randomParts = [];
  for (const part of parts) {
    const partItems = await partService.findPartsByTypeAndRarity(part, rarity);
    const randomNumber = Math.round(Math.random() * (partItems.length - 1));
    randomParts.push(partItems?.[randomNumber]);
  }
  return randomParts;
};

const generateRandomDrakerByRarity = async (rarity) => {
  const attributeObj = getRandomAttribute(rarity);

  const parts = await getRandomPartsByRarity(rarity);
  const partIds = parts.map((item) => item._id);

  const drakerImage = await drakerImageService.findByParts(partIds);

  const drakerClass = getRandomClass();

  const drakerBody = {
    ...attributeObj,
    ...drakerClass,
    parts,
    // drakerImage,
  };
  return drakerBody;
};

export default {
  createDraker,
  updateDrakerById,
  getDrakerById,
  deleteDraker,
  getDrakers,
  transferOwner,
  getDrakerByTokenId,
  getRandomAttribute,
  getRandomParts,
  getRandomClass,
  getDrakerImage,
  getRandomPartsByRarity,
  generateRandomDrakerByRarity,
};
