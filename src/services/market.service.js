import mongoose from 'mongoose';
import ListingItem from '../models/listing-item';
import drakerService from './draker.service';
import drakerImageService from './drakerImage.service';

const getFilterObj = (filter) => {
  for (const [key, value] of Object.entries(filter)) {
    if (Array.isArray(value)) {
      filter[key] = {
        $in: value,
      };
    }
  }

  if (filter.draker) {
    filter.draker = mongoose.Types.ObjectId(filter.draker);
  }
  let { price, ...updatedFilter } = filter;

  if (price) {
    const priceArr = price.split(',');
    const priceFilter = {
      $gte: priceArr[0] === '' ? null : +priceArr[0],
      $lte: priceArr[1] === '' ? null : +priceArr[1],
    };
    let obj = Object.fromEntries(Object.entries(priceFilter).filter(([, v]) => !!v || v === 0));

    if (Object.keys(obj).length > 0) {
      updatedFilter = {
        ...updatedFilter,
        price: obj,
      };
    }
  }

  return updatedFilter;
};

const getSortObj = (options) => {
  let sort = '';
  if (options.sortBy) {
    options.sortBy.split(',').forEach((sortOption) => {
      const [key, order] = sortOption.split(':');
      sort = {
        ...sort,
        [key]: order === 'desc' ? -1 : 1,
      };
    });
  } else {
    sort = { createdAt: -1 };
  }

  return sort;
};

const getDrakersListing = async (filter, option) => {
  try {
    let matchInAggregate = {
      draker: { $ne: null },
    };

    const additionalFilter = getFilterObj(filter);
    if (additionalFilter) {
      matchInAggregate = {
        ...matchInAggregate,
        ...additionalFilter,
      };
    }
    let drakerClassMatch = {};

    if (matchInAggregate.drakerClass) {
      drakerClassMatch = {
        'drakerInfo.drakerClass': matchInAggregate.drakerClass,
      };

      delete matchInAggregate.drakerClass;
    }
    console.log('matchInAggregate', matchInAggregate, drakerClassMatch);
    const sortObj = getSortObj(option);

    const { limit = 10, page = 1 } = option;

    const skip = (page - 1) * limit;

    const items = await ListingItem.aggregate([
      {
        $match: matchInAggregate,
      },
      {
        $sort: sortObj,
      },
      {
        $lookup: {
          from: 'drakers',
          localField: 'draker',
          foreignField: '_id',
          as: 'drakerInfo',
        },
      },
      {
        $unwind: '$drakerInfo',
      },
      {
        $match: drakerClassMatch,
      },
      {
        $facet: {
          metadata: [
            { $count: 'totalResults' },
            {
              $addFields: {
                page: +page,
                limit: +limit,
              },
            },
          ],
          results: [{ $skip: +skip }, { $limit: +limit }],
        },
      },
    ]);

    if (items[0].results.length === 0) {
      return {
        results: [],
      };
    }

    const { totalResults } = items[0].metadata[0];

    items[0].metadata[0] = {
      ...items[0].metadata[0],
      totalPages: Math.ceil(totalResults / limit),
    };
    const mappedResults = [];
    for (const item of items[0].results) {
      const drakerImage = await drakerImageService.findByParts(item.drakerInfo.parts);
      mappedResults.push({
        ...item,
        drakerInfo: {
          ...item.drakerInfo,
          drakerImage: drakerImage.imageUrl,
        },
      });
    }
    return {
      results: mappedResults,
      ...items[0].metadata[0],
    };
  } catch (error) {
    throw error;
  }
};

const getListingItems = async (filter, option) => {
  try {
    let items = await ListingItem.paginate(filter, option);
    items.results = items.results.map(async (item) => {
      if (item.draker?._id) {
        const drakerImage = await drakerImageService.findByParts(item._doc.draker.parts);
        console.log('drakerImage', drakerImage);
        item._doc.drakerInfo = item._doc.draker;
        item._doc.drakerInfo.draker.drakerImage = drakerImage.imageUrl;
      }
      return item;
    });
    return items;
  } catch (error) {
    throw error;
  }
};

const getListingItem = async (listingId) => {
  return ListingItem.findOne({ listingId }).populate('draker');
};

const createListingItem = async (listingItem, session = null) => {
  if (await ListingItem.countDocuments({ listingId: listingItem.listingId })) {
    console.log('createListingItem: already added listing item');
  } else {
    return ListingItem.create([listingItem], { session });
  }
};

const deleteListingItem = async (listingId, session = null) => {
  if (await ListingItem.countDocuments({ listingId })) {
    return ListingItem.deleteOne({ listingId }, { session });
  }
  return null;
};

export default {
  getDrakersListing,
  getListingItems,
  getListingItem,
  createListingItem,
  deleteListingItem,
};
