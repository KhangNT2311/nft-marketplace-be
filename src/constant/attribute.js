export const BURN_ATTR = [3, 6, 9, 12, 18];
export const SUSTAIN_ATTR = [20, 26, 32, 38, 44];
export const CLASS_ATTR = ['fire', 'water', 'earth'];

export const ATTACK_ATTR = [
  [1, 10],
  [5, 15],
  [10, 20],
  [15, 25],
  [25, 35],
];
export const DEFEND_ATTR = [
  [1, 10],
  [5, 15],
  [10, 20],
  [15, 25],
  [25, 35],
];
export const SPEED_ATTR = [
  [1, 10],
  [5, 15],
  [10, 20],
  [15, 25],
  [25, 35],
];
// 000 001 002 010 011 012 020 021 022
export const TAIL_PART = [0, 1, 2];
export const BEAST_PART = [0, 1, 2];
export const BODY_PART = [0, 1, 2];

export const DOPA_ATTR = [
  [2, 80],
  [10, 90],
  [20, 90],
  [30, 100],
  [30, 100],
];
export const BREAK_ATTR = [
  [1, 2],
  [2, 4],
  [4, 6],
  [6, 8],
  [9, 10],
];

export const OPTIMAL_SPEED = [
  [1, 6],
  [4, 10],
  [8, 20],
  [4, 10],
  [1, 20],
];

// export const BASE_RETURN = [
//   [1, 4],
//   [1, 5],
//   [1, 6],
//   [2, 5.5],
//   [4, 6.25],
// ];
