export const BOX_TYPE = {
  0: 'gift',
  1: 'lucky',
};

export const DRAKER_TYPE = {
  0: 'MID1',
  1: 'MID2',
  2: 'BURN1',
  3: 'BURN2',
  4: 'VIPBURN',
};

export const WEEKDAYS = {
  0: 'Sun',
  1: 'Mon',
  2: 'Tue',
  3: 'Wed',
  4: 'Thu',
  5: 'Fri',
  6: 'Sat',
};

const url = process.env.BASE_URL || 'https://api-test.emoves.io';
export const DRAKER_TYPE_IMAGE = {
  0: `${url}/mid1.png`,
  1: `${url}/mid2.png`,
  2: `${url}/burn1.png`,
  3: `${url}/burn2.png`,
  4: `${url}/vipburn.png`,
};

export const DRAKER_TYPES = Object.values(DRAKER_TYPE);

export const TRANSACTION_STATUS = {
  BOUGHT: 'BOUGHT',
  SOLD: 'SOLD',
  CANCEL: 'CANCEL',
  SELL: 'SELL',
  MINT: 'MINT',
  TRANSFER_DRAKERS: 'TRANSFER_DRAKERS',
  CREATE_DRAKERS: 'CREATE_DRAKERS',
  BURN_DRAKERS: 'BURN_DRAKERS',
  ORDER_NFT: 'ORDER_NFT',
};
export const TRANSACTION_STATUSES = Object.values(TRANSACTION_STATUS);

export const MARKET_ITEM_TYPE = {
  GIFT_BOX: 'GIFT_BOX',
  LUCKY_BOX: 'LUCKY_BOX',
  DRAKER: 'DRAKER',
};

export const MARKET_ITEM_TYPES = Object.values(MARKET_ITEM_TYPE);

export const GAMEPLAY_TRANSACTIONS = {
  WITHDRAW: 'WITHDRAW',
  DEPOSIT: 'DEPOSIT',
  REWARD: 'REWARD',
};

export const TOKEN_ERROR_TYPE = {
  EXPIRE: 'TokenExpiredError',
};

export const TOKEN_ERROR_MSG = {
  TOKEN_EXPIRED: 'Token expired',
  INVALID: 'Invalid token',
  SOCKET_AUTH_INVALID: 'Socket Authentication: Invalid token',
};

export const DRAKER_PART = {
  BEAST: 'beast',
  BODY: 'body',
  TAIL: 'tail',
};

export const DRAKER_PARTS = Object.values(DRAKER_PART);
