// eslint-disable-next-line import/prefer-default-export
export const ADDRESSES = {
  marketplace: {
    56: '',
    97: '0x36434a24F0F1eDa8b895a675c8452d2A4D704932',
  },
  multicall: {
    4: '0x5ba1e12693dc8f9c48aad8770482f4739beed696',
    56: '0xfF6FD90A470Aaa0c1B8A54681746b07AcdFedc9B',
    97: '0x8F3273Fb89B075b1645095ABaC6ed17B2d4Bc576',
    137: '0x11ce4B23bD875D7F5C6a31084f55fDe1e9A87507',
  },

  box: {
    56: '',
    97: '0xDc59b5896EEe8F51Ded21db3288AA801D00d9937',
  },

  drakers: {
    56: '',
    97: '0x64a21983747e78c18c625e43d121dfD04e603067',
  },

  sale: {
    56: '',
    97: '0xf27c38195C276c68760230948F7103bECc09EA39',
  },

  boxInteraction: {
    56: '',
    97: '0x399a2d3f98FF44a97e5E04DDD5e3BafE5442A537',
  },
  drt: {
    56: '',
    97: '0x38cDFA8cbe937273956a707528aB7c8a28490567',
  },
  emv: {
    97: '0xD5989d415AcAC38E2832eB5C519390a2AF15B9F6',
  },

  pool: {
    97: '0x36313B960a571Ee6D697B15ee446FD9B1FfEc4ef',
  },
};
