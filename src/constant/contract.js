import ethers from 'ethers';
import BoxInteractionAbi from '../abi/BoxInteraction.json';
import MarketplaceAbi from '../abi/Marketplace.json';
import SaleAbi from '../abi/Sale.json';
import BoxAbi from '../abi/Box.json';
import DrakersAbi from '../abi/Drakers.json';
import PoolAbi from '../abi/Pool.json';

import MulticallAbi from '../abi/Multicall.json';
import { ADDRESSES } from './address';

const rpcs = {
  56: 'https://bsc-dataseed.binance.org/',
  97: 'https://data-seed-prebsc-1-s3.binance.org:8545/',
  137: 'https://polygon-rpc.com/',
};

export const defaultProvider = new ethers.providers.JsonRpcProvider(rpcs[process.env.CHAIN_ID]);

export const getAddress = (name, chainId) => {
  if (chainId) return ADDRESSES[name][chainId];

  return ADDRESSES[name][process.env.CHAIN_ID];
};

export const getContract = (name, abi, chainId, privateKey) => {
  const address = getAddress(name, chainId);
  let provider = defaultProvider;
  if (chainId) {
    provider = new ethers.providers.JsonRpcProvider(rpcs[chainId]);
  }
  if (privateKey) {
    const signer = new ethers.Wallet(privateKey, provider);
    return new ethers.Contract(address, abi, signer);
  }
  return new ethers.Contract(address, abi, provider);
};

export const getMarketplaceContract = (chainId, privateKey) => {
  return getContract('marketplace', MarketplaceAbi, chainId, privateKey);
};

export const getBoxContract = (chainId, privateKey) => {
  return getContract('box', BoxAbi, chainId, privateKey);
};

export const getDrakersContract = (chainId, privateKey) => {
  return getContract('drakers', DrakersAbi, chainId, privateKey);
};

export const getSaleContract = (chainId, privateKey) => {
  return getContract('sale', SaleAbi, chainId, privateKey);
};

export const getBoxInteractionContract = (chainId, privateKey) => {
  return getContract('boxInteraction', BoxInteractionAbi, chainId, privateKey);
};

export const getMulticallContract = (chainId, privateKey) => {
  return getContract('multicall', MulticallAbi, chainId, privateKey);
};

export const getPoolContract = (chainId, privateKey) => {
  return getContract('pool', PoolAbi, chainId, privateKey);
};
