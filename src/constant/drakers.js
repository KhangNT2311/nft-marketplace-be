import { BEAST_PART, BODY_PART, TAIL_PART } from './attribute';

const urlImage = process.env.URL_IMAGE || 'http://localhost:4023';

// const DRAKER_IMAGE = [
//   {
//     type: 'tail',
//     value: 0,
//     data: [
//       {
//         type: 'beast',
//         value: 0,
//         data: [
//           {
//             type: 'body',
//             value: 0,
//             image: `${url}/images/drakers/draker-1.png`,
//           },
//         ],
//       },
//     ],
//   },
// ];

let idx = 1;


const DRAKER_IMAGE = TAIL_PART.map((item) => {
  return {
    type: 'tail',
    value: item,
    data: BEAST_PART.map((beast) => {
      return {
        type: 'beast',
        value: beast,
        data: BODY_PART.map((body) => {
          return {
            type: 'body',
            value: body,
            // eslint-disable-next-line no-plusplus
            image: `${urlImage}/drakers/draker-${idx++}.png`,
          };
        }),
      };
    }),
  };
});

export default DRAKER_IMAGE;
