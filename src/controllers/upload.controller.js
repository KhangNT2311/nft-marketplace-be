import uploadToS3 from '../libs/upload-s3';

const upload = async (req, res) => {
  const { tags = 'notag' } = req.body || {};
  const { file } = req.files || {};

  if (!file) {
    return res.status(400).send({ ok: false, message: 'missing file' });
  }

  const result = await uploadToS3(file, tags, false);

  return res.status(201).send({ ok: true, message: 'created', data: result });
};

const uploadBase64 = async (req, res) => {
  const { tags = 'notag', imageBase64 } = req.body || {};

  if (!imageBase64) {
    return res
      .status(400)
      .send({ ok: false, message: 'missing image with base64 format' });
  }

  const file = imageBase64.startsWith('data')
    ? imageBase64.replace(/^data:image\/\w+;base64,/, '')
    : imageBase64;

  const fileExt = file.charAt(0);

  if (!['/', 'i'].includes(fileExt)) {
    return res.status(400).send({
      ok: false,
      message: 'only support JEPG and PNG file format. please try other type!',
    });
  }

  const result = await uploadToS3(file, tags, true);

  return res.status(201).send({ ok: true, message: 'created', data: result });
};

export default { upload, uploadBase64 };
