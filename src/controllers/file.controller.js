import { uploadFileS3Storage } from "../config/s3";

const upload = async (req, res) => {
  try {
    let promises = [];
    const files = req.files.files;
    const publicIds = req.body.fileUrls;
    const uploadImage = async (file, publicId) => {
      return await uploadFileS3Storage(file, publicId);
    };
    let urls = [];
    if (!Array.isArray(files)) {
      urls = await uploadImage(files, publicIds);
    } else {
      const updatedPublicIds = Array.isArray(publicIds)
        ? publicIds
        : [publicIds];
      files.forEach((file, idx) => {
        promises.push(uploadImage(file, updatedPublicIds?.[idx]));
      });
      urls = await Promise.all(promises);
    }

    res.json({
      success: true,
      message: "Upload files successfully",
      data: urls,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: "Can not upload filesss!",
      error,
    });
  }
};

export default { upload };
