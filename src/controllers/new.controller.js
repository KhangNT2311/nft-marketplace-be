// import pkg from 'bcryptjs';
import httpStatus from 'http-status';
import newService from '../services/new.service';
import ApiError from '../utils/ApiError';
import catchAsync from '../utils/catchAsync';
import { decodeBase64 } from '../utils/commons';
import pick from '../utils/pick';
// const { decodeBase64 } = pkg;
const getNews = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['title']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await newService.queryNews(filter, { ...options, populate: 'author' });
  res.status(200).json({
    data: result,
  });
});

const createNew = catchAsync(async (req, res) => {
  //   console.log('req', req.user);
  const { body } = req;
  const base64 = body.content;
  // const htmlString = decodeBase64(base64);
  const htmlString = decodeBase64(base64);
  // console.log('htmlString', htmlString);
  req.body.content = htmlString;
  const newCreated = await newService.createNew({ ...req.body, author: req.user._id });
  res.status(httpStatus.CREATED).send(newCreated);
});

const getNew = catchAsync(async (req, res) => {
  const newCreated = await newService.getNewById(req.params.newId);
  if (!newCreated) {
    throw new ApiError(httpStatus.NOT_FOUND, 'New not found');
  }
  res.send(newCreated);
});

const updateNew = catchAsync(async (req, res) => {
  if (req.body.content) {
    const { body } = req;
    const base64 = body.content;
    const htmlString = decodeBase64(base64);
    req.body.content = htmlString;
  }
  const newCreated = await newService.updateNewById(req.params.newId, req.body);
  res.send(newCreated);
});

const deleteNew = catchAsync(async (req, res) => {
  await newService.deleteNewById(req.params.newId);
  res.status(200).json({ data: { success: true } });
});

export default {
  getNews,
  createNew,
  deleteNew,
  updateNew,
  getNew,
};
