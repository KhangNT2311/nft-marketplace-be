import pick from '../utils/pick';
import drakerService from '../services/draker.service';

const list = async (req, res) => {
  const filter = pick(req.query, ['owner', 'tokenId', 'drakerType']);
  if (filter.owner) filter.owner = new RegExp(filter.owner, 'ig');
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await drakerService.getDrakers(filter, { ...options, populate: 'parts' });
  return res.status(200).json({ data: result });
};

const createDraker = async (req, res) => {
  const draker = req.body || {};
  const result = await drakerService.createDraker(draker);

  return res.status(200).json({ data: result });
};

const getById = async (req, res) => {
  const drakerId = req.params.drakerId || '';
  const result = await drakerService.getDrakerByTokenId(drakerId);

  return res.status(200).json({ data: result });
};

const updateDraker = async (req, res) => {
  const drakerId = req.params.drakerId || '';

  const body = req.body || {};
  const result = await drakerService.updateDrakerById(drakerId, body);

  return res.status(200).json({ data: result });
};

const generateDraker = async (req, res) => {
  const { rarity } = req.body;
  const draker = await drakerService.generateRandomDrakerByRarity(rarity);
  return res.status(200).json({ data: draker });
};

export default {
  list,
  createDraker,
  getById,
  updateDraker,
  generateDraker,
};
