import httpStatus from 'http-status';
import catchAsync from '../utils/catchAsync';
import { authService, userService, tokenService, emailService, signatureService } from '../services';
import ApiError from '../utils/ApiError';

const getMessage = catchAsync(async (req, res) => {
  console.log('req.params', req.params);
  const user = await userService.getUserById(req.params.id);
  const message = await signatureService.getMessageHash(req.params.id, user?.nonce || 0).catch(() => {
    throw new ApiError(httpStatus.FORBIDDEN, 'Please input valid address');
  });
  res.status(200).send({ message });
});

const connectWallet = catchAsync(async (req, res) => {
  const { signature, id } = req.body;
  const user = await userService.getUserById(id);
  let nonce = 0;
  if (user) {
    nonce = user.nonce;
  }
  await signatureService.verifyAuthenticationMessage(signature, id, nonce);
  const authTokens = await tokenService.generateAuthTokens({ id });
  if (user) {
    user.nonce++;
    await user.save();
  } else {
    await userService.createUser({
      _id: id,
      nonce: 1,
      drtAmount: 0,
    });
  }

  res.status(200).send({ tokens: authTokens });
});

const register = catchAsync(async (req, res) => {
  const user = await userService.createUser(req.body);
  const tokens = await tokenService.generateAuthTokens(user);
  res.status(httpStatus.CREATED).send({ user, tokens });
});

const registerAdmin = catchAsync(async (req, res) => {
  const user = await userService.createUser(req.body);
  const tokens = await tokenService.generateAuthTokens(user);
  res.status(httpStatus.CREATED).send({ user, tokens });
});

const login = catchAsync(async (req, res) => {
  const { email, password } = req.body;
  const user = await authService.loginUserWithEmailAndPassword(email, password);
  const tokens = await tokenService.generateAuthTokens(user);
  res.send({ user, tokens });
});

const loginAdmin = catchAsync(async (req, res) => {
  const { email, password } = req.body;
  const user = await authService.loginUserWithEmailAndPasswordAdmin(email, password);
  const tokens = await tokenService.generateAuthTokens(user);
  res.send({ user, tokens });
});

const logout = catchAsync(async (req, res) => {
  await authService.logout(req.body.refreshToken);
  res.status(httpStatus.NO_CONTENT).send();
});

const refreshTokens = catchAsync(async (req, res) => {
  const tokens = await authService.refreshAuth(req.body.refreshToken);
  res.send({ ...tokens });
});

const forgotPassword = catchAsync(async (req, res) => {
  const resetPasswordToken = await tokenService.generateResetPasswordToken(req.body.email);
  await emailService.sendResetPasswordEmail(req.body.email, resetPasswordToken);
  res.status(httpStatus.NO_CONTENT).send();
});

const resetPassword = catchAsync(async (req, res) => {
  await authService.resetPassword(req.query.token, req.body.password);
  res.status(httpStatus.NO_CONTENT).send();
});

const sendVerificationEmail = catchAsync(async (req, res) => {
  const verifyEmailToken = await tokenService.generateVerifyEmailToken(req.user);
  await emailService.sendVerificationEmail(req.user.email, verifyEmailToken);
  res.status(httpStatus.NO_CONTENT).send();
});

const verifyEmail = catchAsync(async (req, res) => {
  await authService.verifyEmail(req.query.token);
  res.status(httpStatus.NO_CONTENT).send();
});

export default {
  register,
  login,
  logout,
  refreshTokens,
  forgotPassword,
  resetPassword,
  sendVerificationEmail,
  verifyEmail,
  getMessage,
  loginAdmin,
  connectWallet,
  registerAdmin,
};
