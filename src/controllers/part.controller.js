// import pkg from 'bcryptjs';
import httpStatus from 'http-status';
import partService from '../services/part.service';
import ApiError from '../utils/ApiError';
import catchAsync from '../utils/catchAsync';
import { decodeBase64 } from '../utils/commons';
import pick from '../utils/pick';
// const { decodeBase64 } = pkg;
const getParts = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['title', 'rarity', 'type']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await partService.queryParts(filter, { ...options, populate: 'author' });
  res.status(200).json({
    data: result,
  });
});

const createPart = catchAsync(async (req, res) => {
  const partCreated = await partService.createPart({ ...req.body, author: req.user._id });
  res.status(httpStatus.CREATED).send(partCreated);
});

const getPart = catchAsync(async (req, res) => {
  const partCreated = await partService.getPartById(req.params.partId);
  if (!partCreated) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Part not found');
  }
  res.send(partCreated);
});

const updatePart = catchAsync(async (req, res) => {
  if (req.body.content) {
    const { body } = req;
    const base64 = body.content;
    const htmlString = decodeBase64(base64);
    req.body.content = htmlString;
  }
  const partCreated = await partService.updatePartById(req.params.partId, req.body);
  res.send(partCreated);
});

const deletePart = catchAsync(async (req, res) => {
  await partService.deletePartById(req.params.partId);
  res.status(200).json({ data: { success: true } });
});

export default {
  getParts,
  createPart,
  deletePart,
  updatePart,
  getPart,
};
