// import pkg from 'bcryptjs';
import httpStatus from 'http-status';
import drakerImageService from '../services/drakerImage.service';
import ApiError from '../utils/ApiError';
import catchAsync from '../utils/catchAsync';
import { decodeBase64 } from '../utils/commons';
import pick from '../utils/pick';
// const { decodeBase64 } = pkg;
const getDrakerImages = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['parts']);

  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await drakerImageService.queryDrakerImages(filter, { ...options, populate: 'parts' });
  res.status(200).json({
    data: result,
  });
});

const createDrakerImage = catchAsync(async (req, res) => {
  const drakerImageCreated = await drakerImageService.createDrakerImage({ ...req.body, author: req.user._id });
  res.status(httpStatus.CREATED).send(drakerImageCreated);
});

const getDrakerImage = catchAsync(async (req, res) => {
  const drakerImageCreated = await drakerImageService.getDrakerImageById(req.params.drakerImageId);
  if (!drakerImageCreated) {
    throw new ApiError(httpStatus.NOT_FOUND, 'DrakerImage not found');
  }
  res.send(drakerImageCreated);
});

const updateDrakerImage = catchAsync(async (req, res) => {
  if (req.body.content) {
    const { body } = req;
    const base64 = body.content;
    const htmlString = decodeBase64(base64);
    req.body.content = htmlString;
  }
  const drakerImageCreated = await drakerImageService.updateDrakerImageById(req.params.drakerImageId, req.body);
  res.send(drakerImageCreated);
});

const deleteDrakerImage = catchAsync(async (req, res) => {
  await drakerImageService.deleteDrakerImageById(req.params.drakerImageId);
  res.status(200).json({ data: { success: true } });
});

export default {
  getDrakerImages,
  createDrakerImage,
  deleteDrakerImage,
  updateDrakerImage,
  getDrakerImage,
};
