import transactionService from '../services/transaction.service';
import pick from '../utils/pick';

const getUserTransactions = async (req, res) => {
  const user = req.user || {};
  const filter = pick(req.query, ['transactionType']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  if (filter.transactionType === 'ALL') {
    delete filter.transactionType;
  }
  Object.assign(filter, { owner: user.walletAddress });

  const data = await transactionService.getUserTransactions(filter, options);

  return res.status(200).json({ data });
};

const list = async (req, res) => {
  const filter = pick(req.query, ['drakersId', 'type', 'seller']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  console.log('filter', filter);
  const data = await transactionService.queryTransactionsPublic(
    filter,
    options
  );
  console.log('data', data);
  return res.status(200).json({ data });
};

export default {
  getUserTransactions,
  list,
};
