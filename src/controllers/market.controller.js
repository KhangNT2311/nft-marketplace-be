import { ethers } from 'ethers';
import { MARKET_ITEM_TYPE } from '../constant/enums';
import marketService from '../services/market.service';
import userService from '../services/user.service';
import pick from '../utils/pick';
import signatureService from '../services/signature.service';
import partService from '../services/part.service';
import { drakerService } from '../services';

const connectWallet = async (req, res) => {
  const { walletAddress, signature } = req.body;

  const result = await userService.login(walletAddress, signature);

  return res.status(200).send({ ok: true, message: 'succeed', data: result });
};

const createUser = async (req, res) => {
  const { walletAddress } = req.body || {};
  const user = await userService.createUser(walletAddress);

  return res.status(200).json({ data: user });
};

const getUsers = async (req, res) => {
  const walletAddress = req.query.walletAddress;
  const users = await userService.getUsers(walletAddress);

  return res.status(200).json({ success: 'ok', data: users || [] });
};

const getMyDrakers = async (req, res) => {
  const result = await userService.getMyDrakers();

  return res.status(200).json({ success: 'ok', data: result || [] });
};

const getDrakerDetail = async (req, res) => {
  const result = await userService.getDrakerDetail();

  return res.status(200).json({ success: 'ok', data: result || [] });
};

const getDrakers = async (req, res) => {
  const result = await userService.getDrakers();

  return res.status(200).json({ success: 'ok', data: result || [] });
};

const getListingItems = async (req, res) => {
  let filter = pick(req.query, ['owner', 'marketType', 'draker', 'price', 'drakerType', 'paymentToken', 'drakerClass']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  filter = Object.fromEntries(Object.entries(filter).filter(([_, v]) => !!v));
  console.log('filter', filter);
  if (filter.marketType === MARKET_ITEM_TYPE.DRAKER) {
    const result = await marketService.getDrakersListing(filter, {
      ...options,
    });

    return res.status(200).json({ data: result });
  }
  const result = await marketService.getListingItems(filter, {
    ...options,
    populate: 'draker',
  });

  return res.status(200).json({ data: result });
};

const requestOrderNFT = async (req, res) => {
  const { wallet, parts } = req.body;
  let user = await userService.getUserByAddress(wallet);
  if (!user) {
    user = await userService.createUserWithAddress(wallet);
  }
  const partsData = await Promise.all(parts.map((item) => partService.getPartById(item)));
  if (!partsData) return res.status(400).json({ message: 'Something went wrongs...' });
  const price = partsData.reduce((accmu, item) => {
    return (accmu += item.price);
  }, 0);
  const attributeObj = drakerService.getRandomAttribute(1);
  const drakerClass = drakerService.getRandomClass();

  const metadata = {
    owner: wallet,
    parts,
    drakerRarity: 1,
    ...attributeObj,
    ...drakerClass,
  };
  const messageHash = ethers.utils.arrayify(
    ethers.utils.solidityKeccak256(
      ['address', 'uint256', 'address', 'uint256', 'address', 'uint256', 'string'],
      [
        '0x64a21983747e78c18c625e43d121dfD04e603067',
        ethers.utils.parseEther(price.toString()),
        ethers.constants.AddressZero,
        1,
        wallet,
        user.mintNonce,
        JSON.stringify(metadata),
      ]
    )
  );
  const adminSignature = await signatureService.signMessage(process.env.PRIVATE_KEY_AUTHORITY, messageHash);
  res.status(200).json({
    adminSignature,
    nonce: user.mintNonce,
    price,
    rarity: 1,
    wallet,
    paymentToken: ethers.constants.AddressZero,
    metadata: JSON.stringify(metadata),
  });
};

export default {
  connectWallet,
  createUser,
  getUsers,
  getMyDrakers,
  getDrakerDetail,
  getDrakers,
  getListingItems,
  requestOrderNFT,
};
