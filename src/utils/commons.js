export const encodeHtmlString = (htmlString) => {
  const utf8Encode = new TextEncoder();
  const byteArr = utf8Encode.encode(htmlString);
  const base64String = btoa(String.fromCharCode(...new Uint8Array(byteArr)));
  return base64String;
};

export const decodeBase64 = (base64String) => {
  return Buffer.from(base64String, 'base64').toString('utf8');
};
