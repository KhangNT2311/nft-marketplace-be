import Joi from 'joi';
import { objectId } from './custom.validation';

const createNew = {
  body: Joi.object().keys({
    title: Joi.string().required(),
    description: Joi.string().required(),
  }),
};

const getNew = {
  params: Joi.object().keys({
    newId: Joi.string().custom(objectId),
  }),
};

const updateNew = {
  params: Joi.object().keys({
    newId: Joi.required().custom(objectId),
  }),
};

const deleteNew = {
  params: Joi.object().keys({
    newId: Joi.string().custom(objectId),
  }),
};

export default {
  createNew,
  getNew,
  updateNew,
  deleteNew,
};
