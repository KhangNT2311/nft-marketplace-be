import express from 'express';
import authRoute from './auth.route';
import userRoute from './user.route';
import marketRoute from './market.route';
import drakerRoute from './draker.route';
import docsRoute from './docs.route';
import config from '../../config/config';
import newRoute from './new.route';
import fileRoute from './file.route';
import partRoute from './part.route';
import drakerImageRoute from './draker-image.route';
import listenRoute from './listen-transaction.route';

const router = express.Router();

const defaultRoutes = [
  {
    path: '/auth',
    route: authRoute,
  },
  {
    path: '/users',
    route: userRoute,
  },
  {
    path: '/drakers',
    route: drakerRoute,
  },
  {
    path: '/market',
    route: marketRoute,
  },
  {
    path: '/news',
    route: newRoute,
  },
  {
    path: '/files',
    route: fileRoute,
  },
  {
    path: '/parts',
    route: partRoute,
  },
  {
    path: '/draker-images',
    route: drakerImageRoute,
  },
  {
    path: '/listen-transaction',
    route: listenRoute,
  },
];

const devRoutes = [
  // routes available only in development mode
  {
    path: '/docs',
    route: docsRoute,
  },
];

defaultRoutes.forEach((route) => {
  router.use(route.path, route.route);
});

/* istanbul ignore next */
if (config.env === 'development') {
  devRoutes.forEach((route) => {
    router.use(route.path, route.route);
  });
}

export default router;
