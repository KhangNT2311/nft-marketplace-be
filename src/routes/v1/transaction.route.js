import Router from 'express-promise-router';
import transactionController from '../controllers/transaction-controller';
import { authenticate } from '../middlewares/authentication';

const router = new Router();

router.get('/public', transactionController.list);
router.get('/', authenticate, transactionController.getUserTransactions);

export default router;
