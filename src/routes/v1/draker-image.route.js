import express from 'express';
import drakerImageController from '../../controllers/drakerImage.controller';
import auth from '../../middlewares/auth';

const router = express.Router();

router
  .route('/')
  .post(auth('manageDrakerImages'), drakerImageController.createDrakerImage)
  .get(drakerImageController.getDrakerImages);

router
  .route('/:drakerImageId')
  .get(drakerImageController.getDrakerImage)
  .patch(auth('manageDrakerImages'), drakerImageController.updateDrakerImage)
  .delete(auth('manageDrakerImages'), drakerImageController.deleteDrakerImage);

export default router;
