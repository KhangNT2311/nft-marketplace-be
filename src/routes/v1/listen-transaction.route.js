import Router from 'express-promise-router';
import { eventEmitter } from '../../libs/emitter';
const router = new Router();

router.get('/buy-box', async (req, res) => {
  const { transactionId } = req.query;

  eventEmitter.on(`${transactionId}`, (draker) => {
    res.json(draker);
  });
});

export default router;
