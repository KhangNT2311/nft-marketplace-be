import express from 'express';
import newController from '../../controllers/new.controller';
import auth from '../../middlewares/auth';
import validate from '../../middlewares/validate';
import newValidation from '../../validations/new.validation';

const router = express.Router();

router.route('/').post(auth('manageNews'), newController.createNew).get(newController.getNews);

router
  .route('/:newId')
  .get(validate(newValidation.getNew), newController.getNew)
  .patch(auth('manageNews'), validate(newValidation.updateNew), newController.updateNew)
  .delete(auth('manageNews'), validate(newValidation.deleteNew), newController.deleteNew);

export default router;
