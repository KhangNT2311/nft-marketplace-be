import Router from 'express-promise-router';
import fileController from '../../controllers/file.controller';

const router = new Router();

router.post('/upload', fileController.upload);

export default router;
