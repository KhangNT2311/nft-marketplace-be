import Router from 'express-promise-router';
import drakerController from '../../controllers/draker-controller';

const router = new Router();

router.get('/', drakerController.list);
router.get('/:drakerId', drakerController.getById);
router.patch('/:drakerId', drakerController.updateDraker);
router.post('/', drakerController.createDraker);
router.post('/generate', drakerController.generateDraker);

export default router;
