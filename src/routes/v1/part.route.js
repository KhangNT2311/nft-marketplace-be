import express from 'express';
import partController from '../../controllers/part.controller';
import auth from '../../middlewares/auth';

const router = express.Router();

router.route('/').post(auth('manageParts'), partController.createPart).get(partController.getParts);

router
  .route('/:partId')
  .get(partController.getPart)
  .patch(auth('manageParts'), partController.updatePart)
  .delete(auth('manageParts'), partController.deletePart);

export default router;
