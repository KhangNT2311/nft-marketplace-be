import Router from 'express-promise-router';
import marketController from '../../controllers/market.controller';
// import { authenticate } from '../../middlewares/authentication';

const router = new Router();

router.get('/users', marketController.getUsers);
router.post('/users', marketController.createUser);
router.get('/users/nfts', marketController.getDrakers);
router.get('/users/nfts/:nftId', marketController.getDrakerDetail);
// router.get('/users/my-nfts', authenticate, marketController.getMyDrakers);

router.post('/connect', marketController.connectWallet);

router.get('/listing-items', marketController.getListingItems);
router.post('/order-nft', marketController.requestOrderNFT);

export default router;
