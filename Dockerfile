FROM node:alpine

RUN mkdir -p /usr/src/draker-marketplace-be && chown -R node:node /usr/src/draker-marketplace-be

WORKDIR /usr/src/draker-marketplace-be

COPY package.json yarn.lock ./

USER node

RUN yarn install --pure-lockfile

COPY --chown=node:node . .

EXPOSE 4023